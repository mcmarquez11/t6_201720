import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;


import DataStructures.DoubleLinkedList;
import DataStructures.LinearProbingHashST;
import DataStructures.SeparateChainingHashST;
import DataStructures.SequentialSearchST;
import VO.VOTrip;
import VO.VOStop;
import VO.VOStopTime;




public class View {


	private File archivo;
  SeparateChainingHashST<Integer, VOTrip>Trips=new SeparateChainingHashST<Integer,VOTrip>(6007);
  SeparateChainingHashST<Integer, VOStop>Stops=new SeparateChainingHashST<Integer,VOStop>(8001);
  SeparateChainingHashST<Integer, SeparateChainingHashST<Integer,VOStop>>TRIPS=new SeparateChainingHashST<Integer,SeparateChainingHashST<Integer,VOStop>>(6007);
  SeparateChainingHashST<Integer, SeparateChainingHashST<Integer,String>>Tiempos=new SeparateChainingHashST<Integer,SeparateChainingHashST<Integer,String>>(6007);
  LinearProbingHashST<Integer, VOStop> stopsLp= new LinearProbingHashST<Integer, VOStop>(3001);
  LinearProbingHashST<Integer, VOTrip> tripsLp= new LinearProbingHashST<Integer, VOTrip>(6007);
	
	
	public static void main(String args[])
	{  	
	View V1=new View();	
	Scanner sc = new Scanner(System.in);
	
	boolean fin=false;
	while(!fin){
		printMenu();
		
		int option = sc.nextInt();
		
		switch(option){
			
			case 1:
				try {
						V1.loadStopsLp("./data/stops.txt");
						V1.loadStopTimes("./data/stop_times.txt");
						}
				catch(Exception e)
				{				}
				
				System.out.println("Ingrese el stopId de la parada deseada");
				String s3=sc.next();
				int st=(Integer) Integer.parseInt(s3);
				V1.viajesPorParadas(st);
				
			break;
			case 2:
				try{ V1.CargarInformacion();}
				catch (Exception e) {
					// TODO: handle exception
				}
			
				System.out.println("Ingrese el TripId 1");
				String s1=sc.next();
				System.out.println("El TripId dado es"+s1);
				System.out.println("Ingrese el TripId 2");
				String s2 = sc.next();
				System.out.println("El TripId dado es"+s2);
				int a=Integer.parseInt(s1);
				int b=Integer.parseInt(s2);
				V1.ParadasCompartidas(a, b);
				 
		

				
				  					  
				break;
		
		
			    case 3:	
				fin=true;
				sc.close();
				break;
		}
	}
	}
	
public void CargarInformacion() throws IOException 
{
	loadStops("./data/stops.txt");
	loadTrips("./data/trips.txt");
	TRIPS("./data/stop_times.txt");

}


		
public void loadStops(String stopsFile) throws IOException {

	
	archivo = new File (stopsFile);
	if (archivo==null)
	{			throw new IOException("El archivo que se intenta abrir es nulo");
	}
	else
	{
	FileReader reader = new FileReader (archivo);
	
	BufferedReader lector = new BufferedReader(reader);
	String Linea=lector.readLine();
	Linea=lector.readLine();
	while (Linea!=null)
	{
		String[] a= Linea.split(",");
		
		int Id =0;
		int code = 0;
		if(a[0].equals(" "))
		{
			System.out.println(a[2] +" no tiene id");
		}
		else if(a[0].equals(" ")==false)
		{
			Id=Integer.parseInt(a[0]);
		}
		else if(a[1].equals(" "))
		{
			System.out.println(a[2] +" no tiene c�digo");
		}
		else
		{
			code=Integer.parseInt(a[1]);
		}
			
	    String name=a[2];
		double lat=Double.parseDouble(a[4]);
		double lon=Double.parseDouble(a[5]);
	    String zoneId=a[6];
	    DoubleLinkedList<VOStopTime> stopTimes = new DoubleLinkedList<VOStopTime>();
	    VOStop stop= new VOStop(Id, code, name, lat, lon, zoneId, stopTimes);
	    Stops.put(Id, stop);
	    
	    Linea=lector.readLine();
	}
	lector.close();
	}
	
System.out.println("STOPS: "+ Stops.size());
}

   
public void loadTrips(String tripsFile) throws IOException
{	
	archivo = new File (tripsFile);
	if (archivo==null)
	{
		throw new IOException("El archivo que se intenta abrir es nulo");
	}
	else
	{
		
	FileReader reader = new FileReader (archivo);
	
	BufferedReader lector = new BufferedReader(reader);
	String Linea=lector.readLine();
	Linea=lector.readLine();
	
	while (Linea!=null)
	{	
		String[] a= Linea.split(",");

		int route=Integer.parseInt(a[0]);
		int ser=Integer.parseInt(a[1]);
		int trip=Integer.parseInt(a[2]);
	    String Name=a[3];
		int shape=Integer.parseInt(a[7]);
		
		VOTrip t= new VOTrip(route, ser, shape, trip, Name);
		Trips.put(trip, t);		
		Linea=lector.readLine();
	}
	lector.close();
	reader.close();
	
}
	System.out.println("Trips"+Trips.size());}

public void loadStopsLp(String stopsFile) throws IOException
{
	archivo= new File(stopsFile);
	if (archivo==null)
	{			throw new IOException("El archivo que se intenta abrir es nulo");	}
	else
	{
	FileReader reader = new FileReader (archivo);
	
	BufferedReader lector = new BufferedReader(reader);
	String Linea=lector.readLine();
	Linea=lector.readLine();
	while (Linea!=null)
	{
		String[] a= Linea.split(",");
		
		Integer Id =0;
		int code = 0;
		if(a[0].equals(" "))
		{
			System.out.println(a[2] +" no tiene id");
		}
		else if(a[0].equals(" ")==false)
		{
			Id=(Integer) Integer.parseInt(a[0]);
		}
		else if(a[1].equals(" "))
		{
			System.out.println(a[2] +" no tiene c�digo");
		}
		else
		{
			code=Integer.parseInt(a[1]);
		}
			
	    String name=a[2];
		double lat=Double.parseDouble(a[4]);
		double lon=Double.parseDouble(a[5]);
	    String zoneId=a[6];
	    DoubleLinkedList<VOStopTime> stopTimes = new DoubleLinkedList<VOStopTime>();
	    
	    VOStop stop= new VOStop(Id, code, name, lat, lon, zoneId, stopTimes);
	    stopsLp.put(Id, stop);
//
	    Linea=lector.readLine();
	}
	lector.close();

	}
//System.out.println("STOPS (total keys): "+ stopsLp.keys().getSize());
}

public void loadStopTimes(String stopTimesFile) throws IOException
{
	archivo = new File (stopTimesFile);
	if (archivo==null)
	{			throw new IOException("El archivo que se intenta abrir es nulo");
	}
	else
	{
	FileReader reader = new FileReader (archivo);
	
	BufferedReader lector = new BufferedReader(reader);
	String Linea=lector.readLine();
	Linea=lector.readLine();
	while (Linea!=null)
	{
		String[] a= Linea.split(",");
		int tripId=Integer.parseInt(a[0]);
	    String arrTime=a[1];
	    String depTime=a[2];
		Integer stopId=(Integer) Integer.parseInt(a[3]);
		int stopSequence=Integer.parseInt(a[4]);
		double distTraveled=0;
		if(a.length==7)
		{
			distTraveled=Double.parseDouble(a[8]);
		}

	    VOStopTime sTime= new VOStopTime(tripId, arrTime, depTime, stopId, stopSequence, distTraveled);
	    VOStop stop = stopsLp.get(stopId);
	    DoubleLinkedList<VOStopTime> st= stop.stopTimes();
	    st.add(sTime);
	    Linea=lector.readLine();
	}
	}
}

public void TRIPS (String stopTimesFile) throws IOException {
	
	archivo = new File (stopTimesFile);
	if (archivo==null)
	{			throw new IOException("El archivo que se intenta abrir es nulo");
	}
	else
	{
	FileReader reader = new FileReader (archivo);
	
	BufferedReader lector = new BufferedReader(reader);
	String Linea=lector.readLine();
	Linea=lector.readLine();
	String[] a= Linea.split(",");
	SeparateChainingHashST<Integer, VOStop>h1=new SeparateChainingHashST<>(6001);
	SeparateChainingHashST<Integer, String>h2=new SeparateChainingHashST<>(6001);
	int contador=0;
	int k=Integer.parseInt(a[0]);
	while (Linea!=null&&contador<50000)
	{
	        a= Linea.split(",");
		    int tripId=Integer.parseInt(a[0]);
		    if(tripId==k)
		    {
	 
		int stopId=Integer.parseInt(a[3]);
	    String arrTime=a[1];
	

	    VOStop k1= Stops.get(stopId);
	     h1.put(stopId, k1);
	     h2.put(stopId, arrTime);
	     
	     contador++;
		    }
		    else{
		    	
		    	TRIPS.put(k, h1);
		    	Tiempos.put(k,h2);
		    	k=tripId;
		    	h1=new SeparateChainingHashST<>(6001);
		    	h2=new SeparateChainingHashST<>(6001);
		    	int stopId=Integer.parseInt(a[3]);
		    	String arrTime=a[1];

		  	    VOStop k1= Stops.get(stopId);
		  	     h1.put(tripId, k1);
		  	     h2.put(tripId, arrTime);
		  	     
		  	     contador++;
		    }
	    Linea=lector.readLine();
	 
	 
	}
lector.close();
	System.out.println("TRIPS"+TRIPS.size()+" "+contador+"tiempos"+Tiempos.size());
}   
	}

public void viajesPorParadas(int stopId)
{
	DoubleLinkedList<VOStopTime> viajes= stopsLp.get(stopId).stopTimes();
	System.out.println("Hay "+viajes.getSize() + " viajes que usan la parada con Id "+stopId);
	LinearProbingHashST<Integer, String> timesXtrip=new LinearProbingHashST<Integer,String>(1001);
	DoubleLinkedList<Integer> tripsOrd = new DoubleLinkedList<Integer>();
	
	Iterator<VOStopTime> i = viajes.iterator();
	
	while(i.hasNext())
	{
		VOStopTime sTime= i.next();
		timesXtrip.put(sTime.TripId(), sTime.ArrivalTime());
	}
	
	i=viajes.iterator();
	while(i.hasNext()  )
	{
		VOStopTime st= i.next();
		int tripId= st.TripId();
		String arrT=st.ArrivalTime();
		
		boolean termino = false;	
		int j=0;
		while(j<tripsOrd.getSize()&& !termino)
		{
			if(arrT.compareTo(timesXtrip.get(tripsOrd.getElement(j)))>0)
			{
				tripsOrd.AddAtk(j, tripId);
				termino=true;
			}
			j++;
		}
		if(!termino)
		{

			tripsOrd.addAtEnd(tripId);
		}
	}
	
	System.out.println("Los viajes son: ");
	Iterator<Integer> iter = tripsOrd.iterator();
	while(iter.hasNext())
	{
		int t=iter.next();
		System.out.println("Id del viajes: " + t + " ,hora de llegada: " + timesXtrip.get(t));
		
	}

}

	// requerimiento 2.2 .Segundo requerimiento de la parte 2 del Taller 
	public void ParadasCompartidas(int t1, int t2)
	{  System.out.println("paradas compartidas");
		DoubleLinkedList<VOStop>St=new DoubleLinkedList<VOStop>();
	    DoubleLinkedList<String>Times=new DoubleLinkedList<String>();
		SeparateChainingHashST<Integer, VOStop> a1=TRIPS.get(t1);
		SeparateChainingHashST<Integer, VOStop> a2=TRIPS.get(t2);
		SeparateChainingHashST<Integer, String> b1=Tiempos.get(t1);
		SeparateChainingHashST<Integer, String> b2=Tiempos.get(t2);
		DoubleLinkedList<Integer>keys=(DoubleLinkedList<Integer>)a1.keys();
		System.out.println(keys.getSize()+"size");
		Iterator<Integer>i1=keys.iterator();
		while(i1.hasNext())
		{int j=i1.next();
		int StopId=a1.get(j).Id();
		if(a2.get(StopId)!=null)
		{
		//St.add(a1.get(j));
		//Times.add(b1.get(j));
		int jj=0;
		boolean agrego=false;
		while(jj<St.getSize()&&agrego==false)
		{
		  if(Times.getElement(jj).compareTo((b1.get(j)))<0)
			{ 
			  Times.AddAtk(jj, b1.get(j));
			  St.AddAtk(jj, a1.get(j));
			  agrego=true;
			}
			jj++;
		}
		if(agrego==false)
		{
			Times.addAtEnd(b1.get(j));
			St.addAtEnd(a1.get(j));
		}
		}
		
			
			
		}
		System.out.println("////LAS PARADAS COMPARTIDAS SON.///");
		for(int i=0;i<St.getSize();i++)
		{
			System.out.println(St.getElement(i).getName()+"  La hora de llegada es "+Times.getElement(i));
		}
		
		
		
		
	}
private static void printMenu() {
	     System.out.println("$$$ TALLER 6 ESTRUCTURAS DE DATOS $$$");
	     System.out.println("Ingrese un n�mero");
	     System.out.println("1.Requerimieto 2.1");
		 System.out.println("2.Requerimiento2.2 Dar Paradas compartidas por ambos viajes");
		 System.out.println("3.Finaliza programa");
		;
		
	}

}

package DataStructures;

public class SeparateChainingHashST<Key,Value> {

	private int M;
	private int N=0;
	private SequentialSearchST<Key,Value>[] listas;
	DoubleLinkedList<Key>Keys= new DoubleLinkedList<Key>();
	
	public SeparateChainingHashST(int M)
	{
		this.M=M;
		listas = (SequentialSearchST<Key,Value>[]) new SequentialSearchST[M];
		for (int i=0; i<M; i++)
		{
			listas[i]=new SequentialSearchST<Key,Value>();
		}
		
	}
	
	private int hash(Key key)
	{
		return(key.hashCode() & 0x7fffffff)%M; 
				
	}
	public Value get (Key key)
	{
		return(Value) listas[hash(key)].get(key);
		
	}
	public void put (Key key,Value value)
	{
		
		
		
     boolean a=listas[hash(key)].put(key, value);
		if(a==true)
		{N++;
		Keys.addAtEnd(key);		}
		if(N/M>=6)
		{
			Rehash();
			
		}
		
	}
	
	public int size()
	{
		return N;
	}
	
	public void Rehash()
	{    
	
		SeparateChainingHashST<Key, Value> jj=new SeparateChainingHashST<Key,Value>(M*2);
	
	for (int i=0;i<N;i++)
	{
		Key k1=Keys.getElement(i);
		Value V1=get(k1);
		jj.put(k1, V1);
		
	}
	
		listas=jj.lista();
		M=listas.length;
	
		
	}
	public SequentialSearchST<Key,Value>[] lista()
	{return listas;
			}
	
	public Iterable<Key> keys()
	{
	
	return Keys;
		
	}
	public int M()
	{return M;}
	
	
	public void Delete(Key key){
		SequentialSearchST<Key, Value> l=listas[hash(key)];
		Value V=l.get(key);
		int k=0;
		boolean f=false;
	    while(f==false&& k<l.Size())
		{ Value U=l.getElement(k);
	    	if(U==V)
	    	{f=true;}
	    	else{
			k++;}
		}
	    l.deleteATk(k);
		
	   
	}
}

package DataStructures;

public interface IStack<E> {

	public void push (E item);
	
	public E pop();
}

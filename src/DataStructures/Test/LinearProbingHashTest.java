package DataStructures.Test;
import DataStructures.DoubleLinkedList;
import DataStructures.LinearProbingHashST;
import DataStructures.Stack;
import junit.framework.TestCase;

import static org.junit.Assert.*;

import org.junit.Test;


public class LinearProbingHashTest<Key, Value> extends TestCase{ 

	private LinearProbingHashST<Key, Value> gLp;
	private LinearProbingHashST<Integer	, String> lp;
	private DoubleLinkedList<Integer> st;
	private Integer[] keys;
	private String[] vals;

	private Key k1;
	private Key k2;
	private Key k3;
	private Value v1;
	private Value v2;
	private Value v3;
	
	
	public void SetupEscenario1()
	{
		gLp=new LinearProbingHashST<Key, Value>(3);
	}
	
	public void SetupEscenario2()
	{
		lp=new LinearProbingHashST<Integer, String>(9);
		
		lp.put(1, "a");
		lp.put(2, "b");
		lp.put(3, "c");
		lp.put(9, "d");
		lp.put(10, "e");
		lp.put(1, "f");
		
		keys=new Integer[9];
		vals=new String[9];

		keys[9%9]=9;
		vals[9%9]="d";
		
		keys[1%9]=1;
		vals[1%9]="a";
		
		keys[2%9]=2;
		vals[2%9]="b";

		keys[3%9]=3;		
		vals[3%9]="c";

		keys[4]=10;
		vals[4]="e";
	
		keys[1%9]=1;
		vals[1%9]="f";

	}
	
	public void testLinearProbing()
	{
		SetupEscenario1();
		SetupEscenario2();
		
		assertNotNull("la HashST gene�rica deber�a tener 3 elementos", gLp);
		assertNotNull("la HashST de Integer deber�a tener 9 elementos", gLp);

	}
	
	public void testPutGet()
	{
		SetupEscenario2();
		
		for (int i=0; i<keys.length && keys[i]!=null;i++)
		{
			assertEquals("key: "+keys[i], vals[i], lp.get(keys[i]));
		}

	}
	
	public void testKeys()
	{
		SetupEscenario2();
		DoubleLinkedList<Integer> stLp= lp.keys();

		st=new DoubleLinkedList<Integer>();
		for (int i=0; i<keys.length;i++)
		{
			if(keys[i]!=null) st.add(keys[i]);
		}
		assertEquals("deber�an haber "+st.getSize()+ " llaves", st.getSize(), stLp.getSize());
		int i= 0;
		while(i<st.getSize())
		{
			assertEquals("Las llaves no son iguales", st.getElement(i), stLp.getElement(i));
			i++;
		}
	}
	
	public void testRehash()
	{
		SetupEscenario2();
		double lf=lp.keys().getSize()/lp.size();
		assertTrue("factor de carga deber�a ser <0.75 pero es: "+ lf+ "=" +lp.keys().getSize()+"/"+lp.size(), lf<0.75);
	}
	
	public void testDelete()
	{
		SetupEscenario2();
		keys[2]=10;
		vals[2]="e";
		keys[4]=null;
		vals[4]=null;
		
		lp.delete(2);
		assertTrue("la key:2 no deber�a estar en la hashST", lp.contains(2)==false);
		assertNotNull("No se est�n reacomodando las keys luego de eliminar la la key 2",lp.get(10));
	}
}
